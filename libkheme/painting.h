#pragma once

#include <QColor>
#include <QPainter>

#include "theme.h"
#include "animationengine.h"

struct BorderData
{
	qreal rectLeft;
	qreal rectRight;
	qreal rectTop;
	qreal rectBottom;

	qreal rectX;
	qreal rectY;
	qreal rectWidth;
	qreal rectHeight;

	qreal widthLeft;
	qreal widthRight;
	qreal widthTop;
	qreal widthBottom;

	qreal widthCenterX;
	qreal widthCenterY;

	QColor colorLeft;
	QColor colorRight;
	QColor colorTop;
	QColor colorBottom;

	QColor colorCenterX;
	QColor colorCenterY;

	QColor colorBg;

	qreal radiusTopLeftX;
	qreal radiusTopLeftY;
	qreal radiusBottomLeftX;
	qreal radiusBottomLeftY;
	qreal radiusTopRightX;
	qreal radiusTopRightY;
	qreal radiusBottomRightX;
	qreal radiusBottomRightY;

	void paintBackgroundTo(QPainter*);
	void paintBorderTo(QPainter*);
	bool updateFrom(
		ElementStyle* style,
		const QMap<QString, QVariant>& props,
		QMap<QString, QSharedPointer<AnimationEngine>>& engines,
		const QRect& rect,
		const QPalette& palette);
};
