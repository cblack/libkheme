#pragma once

// pokipokilili imports
#include <QDataStream>
#include <QDebug>
#include <QSharedPointer>
#include <QQmlListProperty>

// user type imports
#include <QColor>
#include <QList>
#include <QString>
#include <QPalette>

class Attribute;
class Transition;
class GradientStep;
class BackgroundColor;
class BackgroundGradient;
class BorderColor;
class BorderDashed;
class BorderDotted;
class BorderRounded;
class BorderRoundedEach;
class BorderSolid;
class BorderWidth;
class BorderWidthEach;
class BorderWidthXY;
class Decoration;
class ElementStyle;
class Glow;
class Inset;
class Padding;
class Shadow;
class Theme;

#define SynthesizeProperty(kind, name) \
	Q_PROPERTY(kind name READ name WRITE set_##name NOTIFY name##_changed STORED true) \
private: \
	kind m_##name = {}; \
\
public: \
	Q_SIGNAL void name##_changed(); \
\
public: \
	kind name() const { return m_##name; }; \
\
public: \
	void set_##name(kind value) \
	{ \
		if (m_##name == value) { \
			return; \
		} \
		m_##name = value; \
		Q_EMIT name##_changed(); \
	};

#define SynthesizePropertyDefault(kind, name, default) \
	Q_PROPERTY(kind name READ name WRITE set_##name NOTIFY name##_changed STORED true) \
private: \
	kind m_##name = default; \
\
public: \
	Q_SIGNAL void name##_changed(); \
\
public: \
	kind name() const { return m_##name; }; \
\
public: \
	void set_##name(kind value) \
	{ \
		if (m_##name == value) { \
			return; \
		} \
		m_##name = value; \
		Q_EMIT name##_changed(); \
	};

#define SynthesizeObjectProperty(kind, name) \
	Q_PROPERTY(kind* name READ name WRITE set_##name NOTIFY name##_changed STORED true) \
private: \
	QSharedPointer<kind> m_##name; \
\
public: \
	Q_SIGNAL void name##_changed(); \
\
public: \
	kind* name() const { return m_##name.data(); }; \
\
public: \
	void set_##name(kind* value) \
	{ \
		if (m_##name == value) { \
			return; \
		} \
		m_##name.reset(value); \
		Q_EMIT name##_changed(); \
	};

#define SynthesizeObjectListProperty(self, kind, name) \
	Q_PROPERTY(QList<kind*> json_##name READ name WRITE set_##name NOTIFY name##_changed STORED true) \
private: \
	QList<QSharedPointer<kind>> m_##name = {}; \
\
public: \
	Q_SIGNAL void name##_changed(); \
\
public: \
	QList<kind*> name() const \
	{ \
		QList<kind*> ret; \
		for (auto& ref : m_##name) { \
			ret << ref.data(); \
		}; \
		return ret; \
	}; \
\
public: \
	void set_##name(QList<kind*> value) \
	{ \
		m_##name.clear(); \
		for (auto* val : value) { \
			m_##name << QSharedPointer<kind>(val); \
		}; \
		Q_EMIT name##_changed(); \
	}; \
	Q_PROPERTY(QQmlListProperty<kind> name READ qml_##name STORED false) \
private: \
	static void append_##name(QQmlListProperty<kind>* prop, kind* val) { qobject_cast<self*>(prop->object)->m_##name << QSharedPointer<kind>(val); } \
\
private: \
	static int count_##name(QQmlListProperty<kind>* prop) { return qobject_cast<self*>(prop->object)->m_##name.count(); } \
\
private: \
	static kind* at_##name(QQmlListProperty<kind>* prop, int idx) { return qobject_cast<self*>(prop->object)->m_##name[idx].data(); } \
\
private: \
	static void clear_##name(QQmlListProperty<kind>* prop) { qobject_cast<self*>(prop->object)->m_##name.clear(); } \
	QQmlListProperty<kind> qml_##name() \
	{ \
		return QQmlListProperty(this, this, &self ::append_##name, &self ::count_##name, &self ::at_##name, &self ::clear_##name); \
	};

#define SynthesizeClass(name) \
public: \
	Q_INVOKABLE name(QObject* parent = nullptr); \
\
public: \
	~name();

class Theme : public QObject
{

	SynthesizeClass(Theme)

		Q_OBJECT

		SynthesizeProperty(QString, name)
			SynthesizeProperty(QColor, accent)
				SynthesizeObjectProperty(ElementStyle, buttonStyle)
					SynthesizeObjectProperty(ElementStyle, menuStyle)
						SynthesizeObjectProperty(ElementStyle, menuItemStyle)
							SynthesizeObjectProperty(ElementStyle, menuSeparatorStyle)
								SynthesizeObjectProperty(ElementStyle, menuTitleStyle)

									QJsonObject serialize();
	static Theme* deserialize(const QJsonObject& obj);
};

class ElementStyle : public QObject
{

	SynthesizeClass(ElementStyle)

		Q_OBJECT

		Q_CLASSINFO("DefaultProperty", "attributes")
			SynthesizeObjectListProperty(ElementStyle, Attribute, attributes)
				SynthesizeObjectListProperty(ElementStyle, Decoration, mouseOver)
					SynthesizeObjectListProperty(ElementStyle, Decoration, mouseDown)
						SynthesizeObjectListProperty(ElementStyle, Decoration, focused)

							public : Q_INVOKABLE QObject* getAttribute(const QString& kind, bool mouseDown, bool hover, bool focus, QObject* context);
	QSharedPointer<QObject> getAttributeShared(const QString& kind, bool mouseDown, bool hover, bool focus);
	QList<Transition*> transitions();

	template <typename T>
	QSharedPointer<T> getAttributeCxx(const QString& kind, bool mouseDown, bool hover, bool focus)
	{
		return qSharedPointerObjectCast<T>(getAttributeShared(kind, mouseDown, hover, focus));
	}
};

class Attribute : public QObject
{

	SynthesizeClass(Attribute)

		Q_OBJECT
};

class ColorProvider : public QObject
{

	SynthesizeClass(ColorProvider)

		Q_OBJECT

		public :

		virtual QColor resultColor(const QPalette&) const
	{
		return QColor();
	}
};

class LiteralColor : public ColorProvider
{

	SynthesizeClass(LiteralColor)

		Q_OBJECT

		SynthesizeProperty(QColor, color)

			QColor resultColor(const QPalette&) const override
	{
		return m_color;
	}
};

class PaletteColor : public ColorProvider
{

	SynthesizeClass(PaletteColor)

		Q_OBJECT

		public :

		enum Role {
			Window = QPalette::Window,
			WindowText = QPalette::WindowText,
			Base = QPalette::Base,
			AlternateBase = QPalette::AlternateBase,
			ToolTipBase = QPalette::ToolTipBase,
			ToolTipText = QPalette::ToolTipText,
			PlaceholderText = QPalette::PlaceholderText,
			Text = QPalette::Text,
			Button = QPalette::Button,
			ButtonText = QPalette::ButtonText,
			BrightText = QPalette::BrightText,
			Light = QPalette::Light,
			Midlight = QPalette::Midlight,
			Dark = QPalette::Dark,
			Mid = QPalette::Mid,
			Shadow = QPalette::Shadow,
			Highlight = QPalette::Highlight,
			HighlightedText = QPalette::HighlightedText,
			Link = QPalette::Link,
			LinkVisited = QPalette::LinkVisited,
		};
	Q_ENUM(Role)

	enum Group
	{
		Disabled = QPalette::Disabled,
		Active = QPalette::Active,
		Inactive = QPalette::Inactive,
		Normal = QPalette::Active,
		NoneSet = -1
	};
	Q_ENUM(Group)

	SynthesizeProperty(Role, role)
		SynthesizePropertyDefault(Group, group, NoneSet)

			QColor resultColor(const QPalette& palette) const override
	{
		if (m_group != -1) {
			return palette.color(static_cast<QPalette::ColorGroup>(m_group), static_cast<QPalette::ColorRole>(m_role));
		}
		return palette.color(static_cast<QPalette::ColorRole>(m_role));
	}
};

class MixedColor : public ColorProvider
{

	SynthesizeClass(MixedColor)

		Q_OBJECT

		public :

		SynthesizeObjectProperty(ColorProvider, colorOne)
			SynthesizeObjectProperty(ColorProvider, colorTwo)
				SynthesizeProperty(qreal, mixFactor)

					QColor resultColor(const QPalette& palette) const override;
};

class Transition : public Attribute
{

	SynthesizeClass(Transition)

		Q_OBJECT

		SynthesizeProperty(QString, property)
			SynthesizeProperty(qint32, duration)
				SynthesizeProperty(qint32, easingType)
};

class Padding : public Attribute
{

	SynthesizeClass(Padding)

		Q_OBJECT

		SynthesizeProperty(qint32, left)
			SynthesizeProperty(qint32, right)
				SynthesizeProperty(qint32, top)
					SynthesizeProperty(qint32, bottom)

						QMargins asMargins() const
	{
		return QMargins(m_left, m_top, m_right, m_bottom);
	}
};

class Inset : public Attribute
{

	SynthesizeClass(Inset)

		Q_OBJECT

		SynthesizeProperty(qint32, left)
			SynthesizeProperty(qint32, right)
				SynthesizeProperty(qint32, top)
					SynthesizeProperty(qint32, bottom)
};

class CenterBorderWidths : public Attribute
{
	SynthesizeClass(CenterBorderWidths)

		Q_OBJECT

		SynthesizeProperty(qint32, xWidth)
			SynthesizeProperty(qint32, yWidth)
};

class BorderWidths : public Attribute
{

	SynthesizeClass(BorderWidths)

		Q_OBJECT

		public : Q_INVOKABLE virtual int topWidth() const
	{
		return 0;
	}
	Q_INVOKABLE virtual int leftWidth() const { return 0; }
	Q_INVOKABLE virtual int rightWidth() const { return 0; }
	Q_INVOKABLE virtual int bottomWidth() const { return 0; }
};

class BorderWidth : public BorderWidths
{

	SynthesizeClass(BorderWidth)

		Q_OBJECT

		SynthesizeProperty(qint32, width)

			public : int topWidth() const override
	{
		return m_width;
	}
	int leftWidth() const override { return m_width; }
	int rightWidth() const override { return m_width; }
	int bottomWidth() const override { return m_width; }
};

class BorderWidthXY : public BorderWidths
{

	SynthesizeClass(BorderWidthXY)

		Q_OBJECT

		SynthesizeProperty(qint32, widthX)
			SynthesizeProperty(qint32, widthY)

				public : int topWidth() const override
	{
		return m_widthY;
	}
	int leftWidth() const override { return m_widthX; }
	int rightWidth() const override { return m_widthX; }
	int bottomWidth() const override { return m_widthY; }
};

class BorderWidthEach : public BorderWidths
{

	SynthesizeClass(BorderWidthEach)

		Q_OBJECT

		SynthesizeProperty(qint32, widthTop)
			SynthesizeProperty(qint32, widthLeft)
				SynthesizeProperty(qint32, widthRight)
					SynthesizeProperty(qint32, widthBottom)

						public : int topWidth() const override
	{
		return m_widthTop;
	}
	int leftWidth() const override { return m_widthLeft; }
	int rightWidth() const override { return m_widthRight; }
	int bottomWidth() const override { return m_widthBottom; }
};

class BorderSolid : public Attribute
{

	SynthesizeClass(BorderSolid)

		Q_OBJECT
};

class BorderDashed : public Attribute
{

	SynthesizeClass(BorderDashed)

		Q_OBJECT
};

class BorderDotted : public Attribute
{

	SynthesizeClass(BorderDotted)

		Q_OBJECT
};

class BorderRoundeds : public Attribute
{

	SynthesizeClass(BorderRoundeds)

		Q_OBJECT

		public : Q_INVOKABLE virtual int radiusTopLeft() const
	{
		return 0;
	}
	Q_INVOKABLE virtual int radiusBottomLeft() const { return 0; }
	Q_INVOKABLE virtual int radiusTopRight() const { return 0; }
	Q_INVOKABLE virtual int radiusBottomRight() const { return 0; }
};

class BorderRounded : public BorderRoundeds
{

	SynthesizeClass(BorderRounded)

		Q_OBJECT

		SynthesizeProperty(qint32, radius)

			public : int radiusTopLeft() const override
	{
		return m_radius;
	}
	int radiusBottomLeft() const override { return m_radius; }
	int radiusTopRight() const override { return m_radius; }
	int radiusBottomRight() const override { return m_radius; }
};

class BorderRoundedEach : public BorderRoundeds
{

	SynthesizeClass(BorderRoundedEach)

		Q_OBJECT

		SynthesizeProperty(qint32, topLeftRadius)
			SynthesizeProperty(qint32, bottomLeftRadius)
				SynthesizeProperty(qint32, topRightRadius)
					SynthesizeProperty(qint32, bottomRightRadius)

						public : int radiusTopLeft() const override
	{
		return m_topLeftRadius;
	}
	int radiusBottomLeft() const override { return m_bottomLeftRadius; }
	int radiusTopRight() const override { return m_topRightRadius; }
	int radiusBottomRight() const override { return m_bottomRightRadius; }
};

class Decoration : public Attribute
{

	SynthesizeClass(Decoration)

		Q_OBJECT
};

class Glow : public Decoration
{

	SynthesizeClass(Glow)

		Q_OBJECT

		SynthesizeObjectProperty(ColorProvider, color)
			SynthesizeProperty(float, radius)
};

class Shadow : public Decoration
{

	SynthesizeClass(Shadow)

		Q_OBJECT

		SynthesizeProperty(float, offsetX)
			SynthesizeProperty(float, offsetY)
				SynthesizeProperty(float, blur)
					SynthesizeObjectProperty(ColorProvider, color)
};

class BackgroundColor : public Decoration
{

	SynthesizeClass(BackgroundColor)

		Q_OBJECT

		SynthesizeObjectProperty(ColorProvider, color)
};

class GradientStep : public QObject
{

	SynthesizeClass(GradientStep)

		Q_OBJECT

		SynthesizeProperty(float, position)
			SynthesizeObjectProperty(ColorProvider, color)
};

class BackgroundGradient : public Decoration
{

	SynthesizeClass(BackgroundGradient)

		Q_OBJECT

		SynthesizeProperty(float, angle)
			SynthesizeObjectListProperty(BackgroundGradient, GradientStep, steps)
};

class BorderColor : public Decoration
{

	SynthesizeClass(BorderColor)

		Q_OBJECT

		SynthesizeObjectProperty(ColorProvider, color)
};
