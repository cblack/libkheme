#include <QScopeGuard>

#include "painting.h"

void BorderData::paintBackgroundTo(QPainter* painter)
{
	painter->save();
	auto restorer = qScopeGuard([painter] { painter->restore(); });
	painter->setRenderHint(QPainter::Antialiasing);

	const QRegion horizontalMiddle(
		QRect(this->rectX,
			this->rectY + this->radiusTopLeftY,
			this->rectWidth,
			this->rectHeight - this->radiusTopLeftY - this->radiusBottomLeftY));
	const QRegion horizontalTop(
		QRect(this->rectX + this->radiusTopLeftX,
			this->rectY,
			this->rectWidth - this->radiusTopLeftX - this->radiusTopRightX,
			this->radiusTopLeftY));
	const QRegion horizontalBottom(
		QRect(this->rectX + this->radiusBottomLeftX,
			this->rectBottom - this->radiusBottomLeftY,
			this->rectWidth - this->radiusBottomLeftX - this->radiusBottomRightX,
			this->radiusBottomLeftY));
	const QRegion topLeft(
		QRect(this->rectLeft,
			this->rectTop,
			2 * this->radiusTopLeftX,
			2 * this->radiusTopLeftY),
		QRegion::Ellipse);
	const QRegion topRight(
		QRect(this->rectRight - 2 * this->radiusTopRightX,
			this->rectTop,
			2 * this->radiusTopRightX,
			2 * this->radiusTopRightY),
		QRegion::Ellipse);
	const QRegion bottomLeft(
		QRect(this->rectLeft,
			this->rectBottom - 2 * this->radiusBottomLeftY,
			2 * this->radiusBottomLeftX,
			2 * this->radiusBottomLeftY),
		QRegion::Ellipse);
	const QRegion bottomRight(
		QRect(
			this->rectRight - 2 * this->radiusBottomRightX,
			this->rectBottom - 2 * this->radiusBottomRightY,
			2 * this->radiusBottomRightX,
			2 * this->radiusBottomRightY),
		QRegion::Ellipse);
	const QRegion clipRegion =
		horizontalMiddle.united(horizontalTop)
			.united(horizontalBottom)
			.united(topLeft)
			.united(topRight)
			.united(bottomLeft)
			.united(bottomRight);

	painter->setClipRegion(clipRegion);
	painter->setPen(Qt::NoPen);
	painter->setBrush(this->colorBg);
	painter->drawRect(this->rectX, this->rectY, this->rectWidth, this->rectHeight);
}


void BorderData::paintBorderTo(QPainter* painter)
{
	painter->save();
	auto restorer = qScopeGuard([painter] { painter->restore(); });
	painter->setRenderHint(QPainter::Antialiasing);

	painter->setBrush(Qt::NoBrush);

	if (this->widthCenterX > 0) {
		painter->setPen(QPen(this->colorCenterX, this->widthCenterX));
		painter->drawLine(
			QLineF(
				this->rectLeft,
				qRound((this->rectTop + this->rectBottom) / 2.0) + 0.5,
				this->rectRight,
				qRound((this->rectTop + this->rectBottom) / 2.0) + 0.5));
	}
	if (this->widthCenterY > 0) {
		painter->setPen(QPen(this->colorCenterY, this->widthCenterY));
		painter->drawLine(
			QLineF(
				qRound((this->rectLeft + this->rectRight) / 2.0) + 0.5,
				this->rectTop,
				qRound((this->rectLeft + this->rectRight) / 2.0) + 0.5,
				this->rectBottom));
	}
	if (this->widthTop > 0) {
		painter->setPen(QPen(this->colorTop, this->widthTop));
		painter->drawLine(
			QLineF(
				this->rectLeft + this->radiusTopRightX,
				this->rectTop,
				this->rectRight - this->radiusTopRightX,
				this->rectTop));
		painter->drawArc(QRectF(this->rectLeft,
							 this->rectTop,
							 2 * this->radiusTopLeftX,
							 2 * this->radiusTopLeftY),
			90 * 16,
			90 * 16);
		painter->drawArc(QRectF(this->rectRight - 2 * this->radiusTopRightX,
							 this->rectTop,
							 2 * this->radiusTopRightX,
							 2 * this->radiusTopRightY),
			0,
			90 * 16);
	}
	if (this->widthBottom > 0) {
		painter->setPen(QPen(this->colorBottom, this->widthBottom));
		painter->drawLine(QLineF(this->rectLeft + this->radiusBottomLeftX,
			this->rectBottom,
			this->rectRight - this->radiusBottomRightX,
			this->rectBottom));
		painter->drawArc(QRectF(this->rectLeft,
							 this->rectBottom - 2 * this->radiusBottomLeftY,
							 2 * this->radiusBottomLeftX,
							 2 * this->radiusBottomLeftY),
			180 * 16,
			90 * 16);
		painter->drawArc(QRectF(this->rectRight - 2 * this->radiusBottomRightX,
							 this->rectBottom - 2 * this->radiusBottomRightY,
							 2 * this->radiusBottomRightX,
							 2 * this->radiusBottomRightY),
			270 * 16,
			90 * 16);
	}
	if (this->widthLeft > 0) {
		painter->setPen(QPen(this->colorLeft, this->widthLeft));
		painter->drawLine(QLineF(this->rectLeft,
			this->rectTop + this->radiusTopLeftY, // y
			this->rectLeft,
			this->rectBottom - this->radiusBottomLeftY)); // y
	}
	if (this->widthRight) {
		painter->setPen(QPen(this->colorRight, this->widthRight));
		painter->drawLine(QLineF(this->rectRight,
			this->rectTop + this->radiusTopRightY,
			this->rectRight,
			this->rectBottom - this->radiusBottomRightY));
	}
}

// painting code references:
// - litehtml implementations (particularly qlitehtml)

static Transition* findTransition(const QList<Transition*>& transitions, const QString& prop)
{
	for (auto* trans : transitions) {
		if (trans->property() == prop) {
			return trans;
		}
	}
	return nullptr;
}

bool BorderData::updateFrom(
	ElementStyle* style,
	const QMap<QString, QVariant>& props,
	QMap<QString, QSharedPointer<AnimationEngine>>& engines,
	const QRect& rect,
	const QPalette& palette)
{
	bool needsRepaint = false;
	bool mouseDown = props["mouseDown"].toBool(), hover = props["hover"].toBool(), focus = props["focus"].toBool();

	auto borderWidths = style->getAttributeCxx<BorderWidths>("borderWidth", mouseDown, hover, focus);
	auto radii = style->getAttributeCxx<BorderRoundeds>("borderRadius", mouseDown, hover, focus);
	auto fgColors = style->getAttributeCxx<BorderColor>("borderColor", mouseDown, hover, focus);
	auto bgColors = style->getAttributeCxx<BackgroundColor>("backgroundColor", mouseDown, hover, focus);
	auto centerBorderWidths = style->getAttributeCxx<CenterBorderWidths>("centerBorderWidth", mouseDown, hover, focus);

	QVariantMap data;
	data["backgroundColor"] = bgColors->color()->resultColor(palette);
	const auto plainData = data;
	const auto transitions = style->transitions();

	if (!engines.contains("_dont_run"))
		for (auto item : plainData.keys()) {
			if (!engines.contains(item)) {
				engines[item] = QSharedPointer<AnimationEngine>::create();
				const auto transition = findTransition(transitions, item);
				if (transition != nullptr) {
					engines[item]->setDuration(transition->duration());
					engines[item]->setEasingCurve(static_cast<QEasingCurve::Type>(transition->easingType()));
				}
				engines[item]->setValue(plainData[item]);
			} else {
				engines[item]->setValue(plainData[item]);
				data[item] = engines[item]->value();
			}
		}

	if (!engines.contains("_dont_run"))
		for (const auto& engine : engines) {
			if (engine->running()) {
				needsRepaint = true;
				break;
			}
		}

	this->rectLeft = rect.x() + borderWidths->leftWidth() * 1.5;
	this->rectRight = rect.x() + rect.width() - borderWidths->rightWidth() * 1.5;
	this->rectTop = rect.y() + borderWidths->topWidth() * 1.5;
	this->rectBottom = rect.y() + rect.height() - borderWidths->bottomWidth() * 1.5;

	this->rectX = this->rectLeft;
	this->rectY = this->rectTop;
	this->rectWidth = this->rectRight - this->rectLeft;
	this->rectHeight = this->rectBottom - this->rectTop;

	this->widthLeft = borderWidths->leftWidth();
	this->widthRight = borderWidths->rightWidth();
	this->widthTop = borderWidths->topWidth();
	this->widthBottom = borderWidths->bottomWidth();

	this->widthCenterX = centerBorderWidths->xWidth();
	this->widthCenterY = centerBorderWidths->yWidth();
	this->colorCenterX = fgColors->color()->resultColor(palette);
	this->colorCenterY = fgColors->color()->resultColor(palette);

	this->colorLeft = fgColors->color()->resultColor(palette);
	this->colorRight = fgColors->color()->resultColor(palette);
	this->colorTop = fgColors->color()->resultColor(palette);
	this->colorBottom = fgColors->color()->resultColor(palette);

	this->colorBg = data["backgroundColor"].value<QColor>();

	this->radiusTopLeftX = radii->radiusTopLeft();
	this->radiusTopLeftY = radii->radiusTopLeft();
	this->radiusBottomLeftX = radii->radiusBottomLeft();
	this->radiusBottomLeftY = radii->radiusBottomLeft();
	this->radiusTopRightX = radii->radiusTopRight();
	this->radiusTopRightY = radii->radiusTopRight();
	this->radiusBottomRightX = radii->radiusBottomRight();
	this->radiusBottomRightY = radii->radiusBottomRight();

	return needsRepaint;
}
