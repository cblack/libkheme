#include "animationengine.h"

AnimationEngine::AnimationEngine()
{
	QObject::connect(&_animation, &QVariantAnimation::finished, &_animation, [=] {
		_animation.setStartValue(QVariant());
		_animation.setEndValue(QVariant());
		_animation.setDirection(QAbstractAnimation::Forward);
		_initialized = false;
	});
}
void AnimationEngine::setDuration(int ms)
{
	_animation.setDuration(ms);
}
void AnimationEngine::setValue(const QVariant& value)
{
	if (!_initialized) {
		_state = value;
		_initialized = true;
		_animation.setStartValue(value);
	} else if (_state == value) {
		return;
	} else {
		_state = value;

		if (value == _animation.startValue()) {
			_animation.setDirection(QAbstractAnimation::Backward);
			return;
		}

		_animation.setEndValue(value);

		if (_animation.state() != QAbstractAnimation::Running) {
			_hasStarted = true;
			_animation.start();
		}
	}
}
void AnimationEngine::setEasingCurve(const QEasingCurve& curve)
{
	_animation.setEasingCurve(curve);
}
QVariant AnimationEngine::value()
{
	if (!_hasStarted) {
		return _state;
	}
	return _animation.currentValue();
}
bool AnimationEngine::running()
{
	return _animation.state() == QAbstractAnimation::Running;
}
