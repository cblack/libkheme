#include "libkheme.h"
#include "serializerbase.h"
#include "theme.h"

void Kheme::registerMetatypes()
{
	QtJsonSerializer::registerTypes();

	qRegisterMetaType<Attribute*>();
	qRegisterMetaType<Transition*>();
	qRegisterMetaType<GradientStep*>();
	qRegisterMetaType<BackgroundColor*>();
	qRegisterMetaType<BackgroundGradient*>();
	qRegisterMetaType<BorderColor*>();
	qRegisterMetaType<BorderDashed*>();
	qRegisterMetaType<BorderDotted*>();
	qRegisterMetaType<BorderRounded*>();
	qRegisterMetaType<BorderRoundeds*>();
	qRegisterMetaType<BorderRoundedEach*>();
	qRegisterMetaType<BorderSolid*>();
	qRegisterMetaType<BorderWidth*>();
	qRegisterMetaType<BorderWidths*>();
	qRegisterMetaType<BorderWidthEach*>();
	qRegisterMetaType<BorderWidthXY*>();
	qRegisterMetaType<CenterBorderWidths*>();
	qRegisterMetaType<Decoration*>();
	qRegisterMetaType<ElementStyle*>();
	qRegisterMetaType<Glow*>();
	qRegisterMetaType<Inset*>();
	qRegisterMetaType<Padding*>();
	qRegisterMetaType<Shadow*>();
	qRegisterMetaType<Theme*>();
	qRegisterMetaType<ColorProvider*>();
	qRegisterMetaType<LiteralColor*>();
	qRegisterMetaType<PaletteColor*>();
	qRegisterMetaType<MixedColor*>();
	qRegisterMetaType<PaletteColor::Role>();
	qRegisterMetaType<PaletteColor::Group>();

	QtJsonSerializer::SerializerBase::registerBasicConverters<Attribute*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<Transition*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<GradientStep*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BackgroundColor*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BackgroundGradient*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderColor*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderDashed*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderDotted*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderRounded*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderRoundeds*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderRoundedEach*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderSolid*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderWidth*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderWidths*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderWidthEach*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<BorderWidthXY*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<CenterBorderWidths*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<Decoration*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<ElementStyle*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<Glow*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<Inset*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<Padding*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<Shadow*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<Theme*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<ColorProvider*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<LiteralColor*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<PaletteColor*>();
	QtJsonSerializer::SerializerBase::registerBasicConverters<MixedColor*>();
}
