import qbs.FileInfo

StaticLibrary {
    name: "kheme"

    files: [
        "*.cpp",
        "jsonserializer/*.cpp",
        "jsonserializer/*.h",
        "jsonserializer/typeconverters/*.cpp",
        "jsonserializer/typeconverters/*.h",
    ]

    cpp.includePaths: ["jsonserializer"]
    cpp.cxxLanguageVersion: "c++17"
    cpp.defines: ["QT_NO_LINKED_LIST"]

    Group {
        name: "API Headers"
        files: ["*.h"]
        qbs.install: true
        qbs.installDir: "include"
    }
    Group {
        fileTagsFilter: ["Exporter.qbs.module"]
        qbs.installDir: "qbs/modules/libkheme"
    }
    Export {
        Depends { name: "Qt"; submodules: ["gui","qml"] }
        Depends { name: "cpp" }
        cpp.includePaths: [exportingProduct.sourceDirectory]
        cpp.cxxLanguageVersion: "c++17"
        prefixMapping: [{
            prefix: exportingProduct.sourceDirectory,
            replacement: FileInfo.joinPaths(qbs.installPrefix, "include")
        }]
    }

    install: true
    installDebugInformation: true
    installDir: "lib64"

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core-private", "gui","qml"] }

    Depends { name: "Exporter.qbs" }
    Depends { name: "Exporter.pkgconfig" }
    Exporter.pkgconfig.versionEntry: "0.1"
}
