#pragma once

#include <QVariant>
#include <QVariantAnimation>

class AnimationEngine
{
	bool _initialized = false;
	bool _hasStarted = false;
	QVariant _state;
	QVariantAnimation _animation;

public:
	explicit AnimationEngine();
	void setDuration(int ms);
	void setValue(const QVariant& value);
	void setEasingCurve(const QEasingCurve& curve);
	QVariant value();
	bool running();
};
