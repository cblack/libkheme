#include <QCoreApplication>
#include <QQmlComponent>
#include <QQmlEngine>

#include "libkheme.h"
#include "serializerbase.h"
#include "theme.h"

#define addType(t) qmlRegisterType<t>("org.kde.khemec", 1, 0, #t)

static void registerTypes()
{
	addType(Attribute);
	addType(Transition);
	addType(GradientStep);
	addType(BackgroundColor);
	addType(BackgroundGradient);
	addType(BorderColor);
	addType(BorderDashed);
	addType(BorderDotted);
	addType(BorderRounded);
	addType(BorderRoundeds);
	addType(BorderRoundedEach);
	addType(BorderSolid);
	addType(BorderWidth);
	addType(BorderWidths);
	addType(BorderWidthEach);
	addType(BorderWidthXY);
	addType(CenterBorderWidths);
	addType(Decoration);
	addType(ElementStyle);
	addType(Glow);
	addType(Inset);
	addType(Padding);
	addType(Shadow);
	addType(Theme);
	addType(ColorProvider);
	addType(LiteralColor);
	addType(PaletteColor);
	addType(MixedColor);
}

Theme* Kheme::ThemeCompiler::compileTheme(const QByteArray& dat, const QUrl& src)
{
	static bool registered = false;
	if (!registered) {
		registerTypes();
		registered = true;
	}

	QByteArray data = dat;
	data.prepend("import org.kde.khemec 1.0 \n");
	data.prepend("import QtQuick 2.0\n");

	QQmlEngine eng;
	// eng.setImportPathList({});
	QQmlComponent c(&eng);
	c.setData(data, src);

	Theme* ret = nullptr;

	while (c.status() == QQmlComponent::Loading) {
		QCoreApplication::processEvents();
	}

	if (c.status() == QQmlComponent::Ready) {
		auto it = qobject_cast<Theme*>(c.create());
		if (it == nullptr) {
			qCritical() << c.errorString();
			return nullptr;
		}
		ret = it;
		QCoreApplication::exit(0);
	} else if (c.status() == QQmlComponent::Error) {
		ret = nullptr;
		qCritical() << c.errorString();
	}

	return ret;
}
