#include <QJsonObject>
#include <QQmlEngine>

#include "libkheme.h"
#include "theme.h"
#include "jsonserializer/jsonserializer.h"

#define SynthesizeClassImpl(class, super, hook) \
	class ::class(QObject * parent) : super(parent) { hook } \
	class ::~class() { }

SynthesizeClassImpl(Attribute, QObject, )
	SynthesizeClassImpl(BackgroundColor, Decoration, )
		SynthesizeClassImpl(BackgroundGradient, Decoration, )
			SynthesizeClassImpl(BorderColor, Decoration, )
				SynthesizeClassImpl(BorderDashed, Attribute, )
					SynthesizeClassImpl(BorderDotted, Attribute, )
						SynthesizeClassImpl(BorderRounded, BorderRoundeds, )
							SynthesizeClassImpl(BorderRoundedEach, BorderRoundeds, )
								SynthesizeClassImpl(BorderRoundeds, Attribute, )
									SynthesizeClassImpl(BorderSolid, Attribute, )
										SynthesizeClassImpl(BorderWidth, BorderWidths, )
											SynthesizeClassImpl(BorderWidthEach, BorderWidths, )
												SynthesizeClassImpl(BorderWidths, Attribute, )
													SynthesizeClassImpl(BorderWidthXY, BorderWidths, )
														SynthesizeClassImpl(CenterBorderWidths, Attribute, )
															SynthesizeClassImpl(ColorProvider, QObject, )
																SynthesizeClassImpl(Decoration, Attribute, )
																	SynthesizeClassImpl(ElementStyle, QObject, )
																		SynthesizeClassImpl(Glow, Decoration, )
																			SynthesizeClassImpl(GradientStep, QObject, )
																				SynthesizeClassImpl(Inset, Attribute, )
																					SynthesizeClassImpl(LiteralColor, ColorProvider, )
																						SynthesizeClassImpl(Padding, Attribute, )
																							SynthesizeClassImpl(PaletteColor, ColorProvider, )
																								SynthesizeClassImpl(MixedColor, ColorProvider, )
																									SynthesizeClassImpl(Shadow, Decoration, )
																										SynthesizeClassImpl(Transition, Attribute, )
																											SynthesizeClassImpl(Theme, QObject, m_buttonStyle.reset(new ElementStyle);)

																												QJsonObject Theme::serialize()
{
	Kheme::registerMetatypes();

	QtJsonSerializer::JsonSerializer ser;
	ser.setPolymorphing(QtJsonSerializer::JsonSerializer::Polymorphing::Forced);

	return ser.serialize(this);
}

Theme* Theme::deserialize(const QJsonObject& obj)
{
	Kheme::registerMetatypes();

	QtJsonSerializer::JsonSerializer ser;
	ser.setPolymorphing(QtJsonSerializer::JsonSerializer::Polymorphing::Forced);

	return ser.deserialize<Theme*>(obj);
}

QObject* ElementStyle::getAttribute(const QString& str, bool mouseDown, bool hover, bool focus, QObject* context)
{
	auto it = getAttributeShared(str, mouseDown, hover, focus).data();
	if (auto engine = qmlEngine(context)) {
		engine->setObjectOwnership(it, QQmlEngine::CppOwnership);
	}
	return it;
}

template <typename T>
QSharedPointer<QObject> search(const QString& str, const QSharedPointer<T>& attr)
{
	if (str == "padding") {
		if (const auto padding = qSharedPointerObjectCast<Padding>(attr))
			return padding;
	} else if (str == "inset") {
		if (const auto inset = qSharedPointerObjectCast<Inset>(attr))
			return inset;
	} else if (str == "borderWidth") {
		if (const auto border = qSharedPointerObjectCast<BorderWidths>(attr))
			return border;
	} else if (str == "borderRadius") {
		if (const auto border = qSharedPointerObjectCast<BorderRoundeds>(attr))
			return border;
	} else if (str == "borderColor") {
		if (const auto border = qSharedPointerObjectCast<BorderColor>(attr))
			return border;
	} else if (str == "backgroundColor") {
		if (const auto border = qSharedPointerObjectCast<BackgroundColor>(attr))
			return border;
	} else if (str == "centerBorderWidth") {
		if (const auto centerBorder = qSharedPointerObjectCast<CenterBorderWidths>(attr))
			return centerBorder;
	}
	return QSharedPointer<T>();
};

QSharedPointer<QObject> ElementStyle::getAttributeShared(const QString& str, bool mouseDown, bool hover, bool focus)
{
	static QSharedPointer<Padding> DefaultPadding = QSharedPointer<Padding>::create();
	static QSharedPointer<Inset> DefaultInset = QSharedPointer<Inset>::create();
	static QSharedPointer<BorderWidths> DefaultBorderWidths = QSharedPointer<BorderWidths>::create();
	static QSharedPointer<CenterBorderWidths> DefaultCenterBorderWidths = QSharedPointer<CenterBorderWidths>::create();
	static QSharedPointer<BorderRoundeds> DefaultBorderRoundeds = QSharedPointer<BorderRoundeds>::create();
	static QSharedPointer<BorderColor> DefaultBorderColor = [] {
		auto border = QSharedPointer<BorderColor>::create();
		auto literal = new LiteralColor(nullptr);
		literal->set_color(Qt::transparent);
		border->set_color(literal);
		return border;
	}();
	static QSharedPointer<BackgroundColor> DefaultBackgroundColor = [] {
		auto background = QSharedPointer<BackgroundColor>::create();
		auto literal = new LiteralColor(nullptr);
		literal->set_color(Qt::transparent);
		background->set_color(literal);
		return background;
	}();

	if (mouseDown)
		for (const auto& attr : m_mouseDown) {
			if (auto v = search<Decoration>(str, attr))
				return v;
		}
	if (hover)
		for (const auto& attr : m_mouseOver) {
			if (auto v = search<Decoration>(str, attr))
				return v;
		}
	if (focus)
		for (const auto& attr : m_focused) {
			if (auto v = search<Decoration>(str, attr))
				return v;
		}
	for (const auto& attr : m_attributes) {
		if (auto v = search<Attribute>(str, attr))
			return v;
	}

	if (str == "padding") {
		return DefaultPadding;
	} else if (str == "inset") {
		return DefaultInset;
	} else if (str == "borderWidth") {
		return DefaultBorderWidths;
	} else if (str == "borderRadius") {
		return DefaultBorderRoundeds;
	} else if (str == "borderColor") {
		return DefaultBorderColor;
	} else if (str == "backgroundColor") {
		return DefaultBackgroundColor;
	} else if (str == "centerBorderWidth") {
		return DefaultCenterBorderWidths;
	} else {
		Q_UNREACHABLE();
	}
}

QList<Transition*> ElementStyle::transitions()
{
	QList<Transition*> transes;
	for (auto attr : m_attributes) {
		if (auto trans = qSharedPointerObjectCast<Transition>(attr)) {
			transes << trans.data();
		}
	}
	return transes;
}

static inline qreal linearInterpolation(qreal a, qreal b, qreal factor)
{
	return a + (b - a) * factor;
}

QColor mix(const QColor& c1, const QColor& c2, qreal mixFactor)
{
	if (mixFactor <= 0.0) {
		return c1;
	}
	if (mixFactor >= 1.0) {
		return c2;
	}

	qreal a = linearInterpolation(c1.alphaF(), c2.alphaF(), mixFactor);
	if (a <= 0.0) {
		return Qt::transparent;
	}

	qreal r = qBound(0.0, linearInterpolation(c1.redF() * c1.alphaF(), c2.redF() * c2.alphaF(), mixFactor), 1.0) / a;
	qreal g = qBound(0.0, linearInterpolation(c1.greenF() * c1.alphaF(), c2.greenF() * c2.alphaF(), mixFactor), 1.0) / a;
	qreal b = qBound(0.0, linearInterpolation(c1.blueF() * c1.alphaF(), c2.blueF() * c2.alphaF(), mixFactor), 1.0) / a;

	return QColor::fromRgbF(r, g, b, a);
}

QColor MixedColor::resultColor(const QPalette& palette) const
{
	auto c1 = m_colorOne->resultColor(palette);
	auto c2 = m_colorTwo->resultColor(palette);
	return mix(c1, c2, m_mixFactor);
}
