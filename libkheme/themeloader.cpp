#include <QDataStream>
#include <QDir>
#include <QFileSystemWatcher>
#include <QJsonDocument>
#include <QJsonObject>

#include "libkheme.h"
#include "theme.h"

using namespace Kheme;

struct ThemeLoader::Private
{
	QFileSystemWatcher* fswatcher = nullptr;
	Theme* theme = nullptr;

	Private(ThemeLoader* q)
	{
		fswatcher = new QFileSystemWatcher(q);
		fswatcher->addPath(QDir::homePath() + "/.kheme");
	}
};

ThemeLoader::ThemeLoader(QObject* parent) : QObject(parent), d(new Private(this))
{
	connect(d->fswatcher, &QFileSystemWatcher::fileChanged, this, [this](const QString& path) {
		QFile fi(path);
		if (!fi.open(QFile::ReadOnly)) {
			return;
		}

		d->theme = Theme::deserialize(QJsonDocument::fromJson(fi.readAll()).object());

		Q_EMIT themeChanged();
		fi.close();
	});

	QFile fi(QDir::homePath() + "/.kheme");
	if (!fi.open(QFile::ReadOnly)) {
		return;
	}

	d->theme = Theme::deserialize(QJsonDocument::fromJson(fi.readAll()).object());

	fi.close();
}

ThemeLoader::~ThemeLoader()
{
}

Theme* ThemeLoader::theme() const
{
	return d->theme;
}
