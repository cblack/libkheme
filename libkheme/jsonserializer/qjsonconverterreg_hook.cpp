#include "qtjsonserializer_global.h"

namespace QtJsonSerializer::__private::converter_hooks {

void register_bool_converters();
void register_char_converters();
void register_signed_char_converters();
void register_uchar_converters();
void register_short_converters();
void register_ushort_converters();
void register_int_converters();
void register_uint_converters();
void register_long_converters();
void register_ulong_converters();
void register_qlonglong_converters();
void register_qulonglong_converters();
void register_float_converters();
void register_double_converters();
void register_QObject__converters();
void register_QChar_converters();
void register_QString_converters();
void register_QDate_converters();
void register_QTime_converters();
void register_QDateTime_converters();
void register_QUrl_converters();
void register_QUuid_converters();
void register_QCborValue_converters();
void register_QCborMap_converters();
void register_QCborArray_converters();
void register_QJsonValue_converters();
void register_QJsonObject_converters();
void register_QJsonArray_converters();
void register_QMimeType_converters();
void register_QVersionNumber_converters();
void register_QLocale_converters();
void register_QRegularExpression_converters();
void register_QSize_converters();
void register_QPoint_converters();
void register_QLine_converters();
void register_QRect_converters();
void register_QSizeF_converters();
void register_QPointF_converters();
void register_QLineF_converters();
void register_QRectF_converters();
void register_QByteArray_converters();

}

namespace QtJsonSerializer {

void registerTypes()
{
	static bool wasCalled = false;
	if (wasCalled)
		return;
	wasCalled = true;
	QtJsonSerializer::__private::converter_hooks::register_bool_converters();
	QtJsonSerializer::__private::converter_hooks::register_char_converters();
	QtJsonSerializer::__private::converter_hooks::register_signed_char_converters();
	QtJsonSerializer::__private::converter_hooks::register_uchar_converters();
	QtJsonSerializer::__private::converter_hooks::register_short_converters();
	QtJsonSerializer::__private::converter_hooks::register_ushort_converters();
	QtJsonSerializer::__private::converter_hooks::register_int_converters();
	QtJsonSerializer::__private::converter_hooks::register_uint_converters();
	QtJsonSerializer::__private::converter_hooks::register_long_converters();
	QtJsonSerializer::__private::converter_hooks::register_ulong_converters();
	QtJsonSerializer::__private::converter_hooks::register_qlonglong_converters();
	QtJsonSerializer::__private::converter_hooks::register_qulonglong_converters();
	QtJsonSerializer::__private::converter_hooks::register_float_converters();
	QtJsonSerializer::__private::converter_hooks::register_double_converters();
	QtJsonSerializer::__private::converter_hooks::register_QObject__converters();
	QtJsonSerializer::__private::converter_hooks::register_QChar_converters();
	QtJsonSerializer::__private::converter_hooks::register_QString_converters();
	QtJsonSerializer::__private::converter_hooks::register_QDate_converters();
	QtJsonSerializer::__private::converter_hooks::register_QTime_converters();
	QtJsonSerializer::__private::converter_hooks::register_QDateTime_converters();
	QtJsonSerializer::__private::converter_hooks::register_QUrl_converters();
	QtJsonSerializer::__private::converter_hooks::register_QUuid_converters();
	QtJsonSerializer::__private::converter_hooks::register_QCborValue_converters();
	QtJsonSerializer::__private::converter_hooks::register_QCborMap_converters();
	QtJsonSerializer::__private::converter_hooks::register_QCborArray_converters();
	QtJsonSerializer::__private::converter_hooks::register_QJsonValue_converters();
	QtJsonSerializer::__private::converter_hooks::register_QJsonObject_converters();
	QtJsonSerializer::__private::converter_hooks::register_QJsonArray_converters();
	QtJsonSerializer::__private::converter_hooks::register_QMimeType_converters();
	QtJsonSerializer::__private::converter_hooks::register_QVersionNumber_converters();
	QtJsonSerializer::__private::converter_hooks::register_QLocale_converters();
	QtJsonSerializer::__private::converter_hooks::register_QRegularExpression_converters();
	QtJsonSerializer::__private::converter_hooks::register_QSize_converters();
	QtJsonSerializer::__private::converter_hooks::register_QPoint_converters();
	QtJsonSerializer::__private::converter_hooks::register_QLine_converters();
	QtJsonSerializer::__private::converter_hooks::register_QRect_converters();
	QtJsonSerializer::__private::converter_hooks::register_QSizeF_converters();
	QtJsonSerializer::__private::converter_hooks::register_QPointF_converters();
	QtJsonSerializer::__private::converter_hooks::register_QLineF_converters();
	QtJsonSerializer::__private::converter_hooks::register_QRectF_converters();
	QtJsonSerializer::__private::converter_hooks::register_QByteArray_converters();
}

}
