#include "qtjsonserializer_global.h"
#include "serializerbase.h"
#include <QtCore/QtCore>

#define QT_JSON_SERIALIZER_NAMED(T) #T

namespace QtJsonSerializer::__private::converter_hooks {

void register_QChar_converters()
{
	SerializerBase::registerBasicConverters<QChar>();
}

}
