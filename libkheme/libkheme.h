#pragma once

#include <QByteArray>
#include <QUrl>
#include <QObject>

class Theme;

namespace Kheme {

void registerMetatypes();

class ThemeLoader : public QObject
{

	Q_OBJECT

	Q_PROPERTY(Theme* theme READ theme NOTIFY themeChanged)

	struct Private;
	QScopedPointer<Private> d;

public:
	ThemeLoader(QObject* parent = nullptr);
	~ThemeLoader();

	Theme* theme() const;
	Q_SIGNAL void themeChanged();
};

class ThemeCompiler
{
public:
	Theme* compileTheme(const QByteArray& data, const QUrl& src);
};

}
