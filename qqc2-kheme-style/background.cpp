#include <QPainter>
#include <QScopeGuard>
#include <QEasingCurve>
#include <QDateTime>

#define protected public
#include <QVariantAnimation>
#undef protected

#include "background.h"
#include "theme.h"
#include "animationengine.h"
#include "painting.h"

struct KhemeBackground::Private
{
	ElementStyle* elementStyle = nullptr;
	QVariantMap props;
	QMap<QString, QSharedPointer<AnimationEngine>> engines;
	QPalette palette;
};

KhemeBackground::KhemeBackground(QQuickItem* parent) : QQuickPaintedItem(parent), d(new Private)
{
}

KhemeBackground::~KhemeBackground()
{
}

void KhemeBackground::paint(QPainter* painter)
{
	Q_ASSERT(d->elementStyle);

	BorderData dat;
	if (dat.updateFrom(d->elementStyle, d->props, d->engines, QRect(0, 0, width(), height()), palette())) {
		update();
	}
	dat.paintBackgroundTo(painter);
	dat.paintBorderTo(painter);
}

ElementStyle* KhemeBackground::elementStyle() const
{
	return d->elementStyle;
}

void KhemeBackground::setElementStyle(ElementStyle* style)
{
	if (d->elementStyle == style)
		return;

	d->elementStyle = style;
	Q_EMIT elementStyleChanged();
}

QVariantMap KhemeBackground::props() const
{
	return d->props;
}

void KhemeBackground::setProps(const QVariantMap& props)
{
	update();

	d->props = props;
	Q_EMIT propsChanged();
}

QPalette KhemeBackground::palette()
{
	return d->palette;
}

void KhemeBackground::setPalette(const QPalette& palette)
{
	if (d->palette == palette)
		return;

	d->palette = palette;
	Q_EMIT paletteChanged();
}
