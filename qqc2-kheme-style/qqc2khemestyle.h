#pragma once

#include <QQmlExtensionPlugin>

class QQC2KhemeStylePlugin : public QQmlExtensionPlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
	QQC2KhemeStylePlugin(QObject *parent = nullptr);
	~QQC2KhemeStylePlugin();

	QString name() const;

	void registerTypes(const char *uri) override;
};
