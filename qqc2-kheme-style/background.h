#pragma once

#include <QQuickItem>
#include <QQuickPaintedItem>

#include "theme.h"

struct BorderData;

class KhemeBackground : public QQuickPaintedItem
{

	Q_OBJECT

	struct Private;
	QScopedPointer<Private> d;

public:
	explicit KhemeBackground(QQuickItem* parent = nullptr);
	~KhemeBackground();

	void paint(QPainter* painter) override;

	Q_PROPERTY(ElementStyle* elementStyle READ elementStyle WRITE setElementStyle NOTIFY elementStyleChanged REQUIRED)
	ElementStyle* elementStyle() const;
	void setElementStyle(ElementStyle* style);
	Q_SIGNAL void elementStyleChanged();

	Q_PROPERTY(QVariantMap props READ props WRITE setProps NOTIFY propsChanged REQUIRED)
	QVariantMap props() const;
	void setProps(const QVariantMap& props);
	Q_SIGNAL void propsChanged();

	Q_PROPERTY(QPalette palette READ palette WRITE setPalette NOTIFY paletteChanged REQUIRED)
	QPalette palette();
	void setPalette(const QPalette& palette);
	Q_SIGNAL void paletteChanged();
};
