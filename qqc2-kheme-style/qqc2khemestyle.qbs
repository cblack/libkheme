DynamicLibrary {
    name: "qqc2khemestyleplugin"

    install: true
    installDir: "lib64/qml/org/kde/kheme"

    Group {
        files: ["qmldir"]
        qbs.install: true
        qbs.installDir: "lib64/qml/org/kde/kheme"
    }
    Group {
        files: ["qqc2-style/**"]
        qbs.install: true
        qbs.installSourceBase: "qqc2-style/"
        qbs.installDir: "lib64/qml/QtQuick/Controls.2/org.kde.kheme"
    }

    files: [
        "*.cpp",
        "*.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "kheme" }
    Depends { name: "Qt"; submodules: ["qml", "quickcontrols2"] }
}