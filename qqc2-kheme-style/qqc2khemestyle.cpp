#include <QQmlEngine>

#include "qqc2khemestyle.h"
#include "libkheme.h"
#include "background.h"

QQC2KhemeStylePlugin::QQC2KhemeStylePlugin(QObject *parent) : QQmlExtensionPlugin(parent)
{
}

QQC2KhemeStylePlugin::~QQC2KhemeStylePlugin()
{
}

QString QQC2KhemeStylePlugin::name() const
{
	return "org.kde.kheme";
}

void QQC2KhemeStylePlugin::registerTypes(const char *uri)
{
	Q_ASSERT(name() == uri);

	qmlRegisterModule(uri, 1, 0);

	Kheme::registerMetatypes();

	qmlRegisterSingletonType<Kheme::ThemeLoader>(uri, 1, 0, "Theme", [](QQmlEngine *, QJSEngine *) -> QObject * { return new Kheme::ThemeLoader; });
	qmlRegisterType<KhemeBackground>(uri, 1, 0, "Background");

	qmlProtectModule(uri, 1);
}
