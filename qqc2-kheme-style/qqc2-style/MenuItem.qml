import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Templates 2.15 as T
import org.kde.kirigami 2.14 as Kirigami

import org.kde.kheme 1.0 as Kheme
import "private" as Private

T.MenuItem {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding,
                            implicitIndicatorWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding,
                             implicitIndicatorHeight + topPadding + bottomPadding)

    topPadding: Kheme.Theme.theme.menuItemStyle.getAttribute("padding", false, false, false, this).top
    bottomPadding: Kheme.Theme.theme.menuItemStyle.getAttribute("padding", false, false, false, this).bottom
    leftPadding: Kheme.Theme.theme.menuItemStyle.getAttribute("padding", false, false, false, this).left
    rightPadding: Kheme.Theme.theme.menuItemStyle.getAttribute("padding", false, false, false, this).right

    background: Kheme.Background {
        elementStyle: Kheme.Theme.theme.menuItemStyle
        props: {
            "mouseDown": control.down,
            "hover": control.hovered,
            "focus": control.visualFocus,
        }
    }

    Shortcut {
        id: controlShortcut
        sequence: (shortcut.visible && controlRoot.action !== null) ? control.action.shortcut : ""
    }

    contentItem: RowLayout {
        Kirigami.Icon {
            Layout.alignment: Qt.AlignVCenter
            visible: control.icon !== undefined && (control.icon.name.length > 0 || control.icon.source.toString().length > 0)
            source: control.icon ? (control.icon.name || control.icon.source) : ""
            color: control.icon ? control.icon.color : "transparent"
            Layout.preferredHeight: Kirigami.Units.iconSizes.smallMedium
            Layout.preferredWidth: Layout.preferredHeight
        }
        QQC2.Label {
            id: label
            Layout.alignment: Qt.AlignVCenter
            Layout.fillWidth: true

            text: control.text
            font: control.font
            color: Kirigami.Theme.textColor
            elide: Text.ElideRight
            visible: control.text
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
        }
        QQC2.Label {
            id: shortcut
            Layout.alignment: Qt.AlignVCenter
            visible: control.action && control.action.shortcut !== undefined

            text: visible ? controlShortcut.nativeText : ""
            font: control.font
            color: label.color
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }
        Item {
            Layout.preferredWidth: Kirigami.Units.smallSpacing
        }
    }

    arrow: Kirigami.Icon {
        x: control.mirrored ? control.padding : control.width - width - control.padding
        y: control.topPadding + (control.availableHeight - height) / 2
        source: control.mirrored ? "go-next-symbolic-rtl" : "go-next-symbolic"
        width: Kirigami.Units.iconSizes.small
        height: width
        visible: control.subMenu
    }

    indicator: null // TODO: checkbox/radiobutton
}
