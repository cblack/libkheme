import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Templates 2.15 as T
import org.kde.kirigami 2.14 as Kirigami

import org.kde.kheme 1.0 as Kheme
import "private" as Private

T.MenuSeparator {
    id: control

    topPadding: Kheme.Theme.theme.menuSeparatorStyle.getAttribute("padding", false, false, false, this).top
    bottomPadding: Kheme.Theme.theme.menuSeparatorStyle.getAttribute("padding", false, false, false, this).bottom
    leftPadding: Kheme.Theme.theme.menuSeparatorStyle.getAttribute("padding", false, false, false, this).left
    rightPadding: Kheme.Theme.theme.menuSeparatorStyle.getAttribute("padding", false, false, false, this).right

    background: Kheme.Background {
        elementStyle: Kheme.Theme.theme.menuSeparatorStyle
        props: {
            "mouseDown": control.down,
            "hover": control.hovered,
            "focus": control.visualFocus,
        }
    }
}
