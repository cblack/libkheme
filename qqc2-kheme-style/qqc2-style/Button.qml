import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import org.kde.kirigami 2.14 as Kirigami

import org.kde.kheme 1.0 as Kheme
import "private" as Private

T.Button {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding,
                            implicitIndicatorWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding,
                             implicitIndicatorHeight + topPadding + bottomPadding)

    topPadding: Kheme.Theme.theme.buttonStyle.getAttribute("padding", false, false, false, this).top
    bottomPadding: Kheme.Theme.theme.buttonStyle.getAttribute("padding", false, false, false, this).bottom
    leftPadding: Kheme.Theme.theme.buttonStyle.getAttribute("padding", false, false, false, this).left
    rightPadding: Kheme.Theme.theme.buttonStyle.getAttribute("padding", false, false, false, this).right

    contentItem: Text {
        text: control.text
    }

    background: Kheme.Background {
        elementStyle: Kheme.Theme.theme.buttonStyle
        props: {
            "mouseDown": control.down,
            "hover": control.hovered,
            "focus": control.visualFocus,
        }
    }
}
