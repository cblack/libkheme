import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Templates 2.15 as T
import org.kde.kirigami 2.14 as Kirigami

import org.kde.kheme 1.0 as Kheme
import "private" as Private

T.Menu {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding)

    topPadding: Kheme.Theme.theme.menuStyle.getAttribute("padding", false, false, false, this).top
    bottomPadding: Kheme.Theme.theme.menuStyle.getAttribute("padding", false, false, false, this).bottom
    leftPadding: Kheme.Theme.theme.menuStyle.getAttribute("padding", false, false, false, this).left
    rightPadding: Kheme.Theme.theme.menuStyle.getAttribute("padding", false, false, false, this).right

    delegate: MenuItem {
    }

    background: Kheme.Background {
        elementStyle: Kheme.Theme.theme.menuStyle
        props: {
            "mouseDown": control.down,
            "hover": control.hovered,
            "focus": control.visualFocus,
        }
    }

    contentItem: ListView {
        implicitHeight: contentHeight
        model: control.contentModel

        highlight: null
        implicitWidth: {
            let maxWidth = 0;
            for (let i = 0; i < contentItem.children.length; ++i) {
                maxWidth = Math.max(maxWidth, contentItem.children[i].implicitWidth);
            }
            return maxWidth;
        }

        spacing: 0

        interactive: QQC2.ApplicationWindow.window ? contentHeight > QQC2.ApplicationWindow.window.height : false
        clip: true
        currentIndex: control.currentIndex || 0
        keyNavigationEnabled: true
        keyNavigationWraps: true

        QQC2.ScrollBar.vertical: QQC2.ScrollBar {}
    }
}
