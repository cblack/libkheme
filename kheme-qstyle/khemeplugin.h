#pragma once

#include <QStylePlugin>

class KhemeStylePlugin : public QStylePlugin
{

	Q_OBJECT
	Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QStyleFactoryInterface" FILE "khemeplugin.json")

public:
	QStyle* create(const QString& key);
};
