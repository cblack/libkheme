DynamicLibrary {
    name: "kheme-qstyle"
    targetName: "kheme"

    install: true
    installDir: "lib64/plugins/styles"

    files: [
        "*.cpp",
        "*.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "kheme" }
    Depends { name: "Qt"; submodules: ["widgets"] }
}