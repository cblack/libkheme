#include <QStyleFactory>
#include <QStyleOption>

#include "libkheme.h"
#include "theme.h"
#include "khemestyle.h"
#include "animationengine.h"
#include "painting.h"

struct KhemeStyle::Private
{
	QMap<const QWidget*, QMap<QString, QSharedPointer<AnimationEngine>>> engines;
	QMap<QString, QSharedPointer<AnimationEngine>> dummy = {{"_dont_run", QSharedPointer<AnimationEngine>(nullptr)}};
	;
	QSharedPointer<Kheme::ThemeLoader> __themeLoader;

	// hack to avoid constructing before QApplication is fully constructed
	QSharedPointer<Kheme::ThemeLoader>& themeLoader()
	{
		if (__themeLoader == nullptr) {
			__themeLoader.reset(new Kheme::ThemeLoader());
		}
		return __themeLoader;
	}
	QMap<QString, QSharedPointer<AnimationEngine>>& getFor(const QWidget* widget)
	{
		if (widget == nullptr) {
			return dummy;
		}
		if (!engines.contains(widget)) {
			engines[widget] = {};
		}
		return engines[widget];
	}
};

KhemeStyle::KhemeStyle(const QString& key) : QProxyStyle(QStyleFactory::create("breeze")), d(new Private)
{
	Q_UNUSED(key)
}

KhemeStyle::~KhemeStyle()
{
}

QMap<QString, QVariant> propsFrom(const QStyleOption* option)
{
	QMap<QString, QVariant> ret;

	ret["mouseDown"] = option->state.testFlag(QStyle::State_Sunken);
	ret["hover"] = option->state.testFlag(QStyle::State_MouseOver) || (qstyleoption_cast<const QStyleOptionMenuItem*>(option) ? option->state.testFlag(QStyle::State_Selected) : false);
	ret["focus"] = option->state.testFlag(QStyle::State_HasFocus);

	return ret;
}

static QRect centerRect(const QRect& rect, int width, int height)
{
	return QRect(rect.left() + (rect.width() - width) / 2, rect.top() + (rect.height() - height) / 2, width, height);
}

static QRect centerRect(const QRect& rect, const QSize& size)
{
	return centerRect(rect, size.width(), size.height());
}

static QPixmap coloredIcon(const QIcon& icon, const QPalette& palette, const QSize& size, QIcon::Mode mode, QIcon::State state)
{
	// const QPalette activePalette = KIconLoader::global()->customPalette();
	// const bool changePalette = activePalette != palette;
	// if (changePalette) {
	//     KIconLoader::global()->setCustomPalette(palette);
	// }
	const QPixmap pixmap = icon.pixmap(size, mode, state);
	// if (changePalette) {
	//     if (activePalette == QPalette()) {
	//         KIconLoader::global()->resetPalette();
	//     } else {
	//         KIconLoader::global()->setCustomPalette(activePalette);
	//     }
	// }
	return pixmap;
}

void KhemeStyle::drawControl(ControlElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget) const
{
	switch (element) {
	case CE_PushButtonBevel: {
		const auto el = d->themeLoader()->theme()->buttonStyle();

		BorderData dat;
		if (dat.updateFrom(el, propsFrom(option), d->getFor(widget), option->rect, option->palette)) {
			const_cast<QWidget*>(widget)->update();
		}
		dat.paintBackgroundTo(painter);
		dat.paintBorderTo(painter);

		return;
	}
	case CE_MenuItem: {
		const auto menuItemOption = qstyleoption_cast<const QStyleOptionMenuItem*>(option);

		switch (menuItemOption->menuItemType) {
		case QStyleOptionMenuItem::MenuItemType::Separator: {
			const auto el = d->themeLoader()->theme()->menuSeparatorStyle();
			BorderData dat;
			if (dat.updateFrom(el, propsFrom(option), d->dummy, option->rect, option->palette)) {
				const_cast<QWidget*>(widget)->update();
			}
			dat.paintBackgroundTo(painter);
			dat.paintBorderTo(painter);
		}
		case QStyleOptionMenuItem::MenuItemType::Normal: {
			// draw themed stuff
			const auto el = d->themeLoader()->theme()->menuItemStyle();
			BorderData dat;
			if (dat.updateFrom(el, propsFrom(option), d->dummy, option->rect, option->palette)) {
				const_cast<QWidget*>(widget)->update();
			}
			dat.paintBackgroundTo(painter);
			dat.paintBorderTo(painter);

			const auto padding = qSharedPointerObjectCast<Padding>(el->getAttributeShared("padding", false, false, false));
			auto contentsRect = option->rect.marginsRemoved(padding->asMargins());
			const bool enabled = option->state.testFlag(State_Enabled);
			const bool sunken = enabled && option->state.testFlag(State_Sunken);
			const bool reverseLayout = option->direction == Qt::RightToLeft;

			// draw icon, text, etc.

			// define relevant rectangles
			// checkbox
			// QRect checkBoxRect;
			// if (menuItemOption->menuHasCheckableItems) {
			// 	checkBoxRect = QRect(contentsRect.left(),
			// 						contentsRect.top() + (contentsRect.height() - Metrics::CheckBox_Size) / 2,
			// 						Metrics::CheckBox_Size,
			// 						Metrics::CheckBox_Size);
			// 	contentsRect.setLeft(checkBoxRect.right() + Metrics::MenuItem_ItemSpacing + 1);
			// }

			// // render checkbox indicator
			// if (menuItemOption->checkType == QStyleOptionMenuItem::NonExclusive) {
			// 	checkBoxRect = visualRect(option, checkBoxRect);

			// 	// checkbox state

			// 	CheckBoxState state(menuItemOption->checked ? CheckOn : CheckOff);
			// 	const bool active(menuItemOption->checked);
			// 	const auto shadow(_helper->shadowColor(palette));
			// 	const auto color(_helper->checkBoxIndicatorColor(palette, false, enabled && active));
			// 	_helper->renderCheckBoxBackground(painter, checkBoxRect, palette, state, false, sunken);
			// 	_helper->renderCheckBox(painter, checkBoxRect, palette, false, state, state, false, sunken);

			// } else if (menuItemOption->checkType == QStyleOptionMenuItem::Exclusive) {
			// 	checkBoxRect = visualRect(option, checkBoxRect);

			// 	const bool active(menuItemOption->checked);
			// 	_helper->renderRadioButtonBackground(painter, checkBoxRect, palette, active ? RadioOn : RadioOff, false, sunken);
			// 	_helper->renderRadioButton(painter, checkBoxRect, palette, false, active ? RadioOn : RadioOff, false, sunken);
			// }

			// icon
			int iconWidth = menuItemOption->maxIconWidth;

			QRect iconRect;
			if (iconWidth > 0) {
				iconRect = QRect(contentsRect.left(), contentsRect.top() + (contentsRect.height() - iconWidth) / 2, iconWidth, iconWidth);
				contentsRect.setLeft(iconRect.right());
				const QSize iconSize(pixelMetric(PM_SmallIconSize, option, widget), pixelMetric(PM_SmallIconSize, option, widget));
				iconRect = centerRect(iconRect, iconSize);
			} else {
				contentsRect.setLeft(contentsRect.left());
			}

			if (!menuItemOption->icon.isNull()) {
				iconRect = visualRect(option, iconRect);

				// icon mode
				QIcon::Mode mode;
				if (enabled)
					mode = QIcon::Normal;
				else
					mode = QIcon::Disabled;

				// icon state
				const QIcon::State iconState(sunken ? QIcon::On : QIcon::Off);
				const QPixmap icon = coloredIcon(menuItemOption->icon, menuItemOption->palette, iconRect.size(), mode, iconState);
				painter->drawPixmap(iconRect, icon);
			}

			// arrow
			const auto width = 16;
			QRect arrowRect(contentsRect.right() - width + 1,
				contentsRect.top() + (contentsRect.height() - width) / 2,
				width,
				width);
			contentsRect.setRight(arrowRect.left() - 1);

			if (menuItemOption->menuItemType == QStyleOptionMenuItem::SubMenu) {
				// // apply right-to-left layout
				// arrowRect = visualRect(option, arrowRect);

				// // arrow orientation
				// const ArrowOrientation orientation(reverseLayout ? ArrowLeft : ArrowRight);

				// // color
				// const QColor arrowColor = _helper->arrowColor(palette, QPalette::WindowText);

				// // render
				// _helper->renderArrow(painter, arrowRect, arrowColor, orientation);
			}

			// text
			auto textRect = contentsRect;
			if (!menuItemOption->text.isEmpty()) {
				// adjust textRect
				QString text = menuItemOption->text;
				textRect = centerRect(textRect, textRect.width(), option->fontMetrics.size(0, text).height());
				textRect = visualRect(option, textRect);

				// set font
				painter->setFont(menuItemOption->font);

				// color role
				const QPalette::ColorRole role = QPalette::WindowText;

				// locate accelerator and render
				const int tabPosition(text.indexOf(QLatin1Char('\t')));
				if (tabPosition >= 0) {
					const int textFlags(Qt::AlignVCenter | Qt::AlignRight);
					QString accelerator(text.mid(tabPosition + 1));
					text = text.left(tabPosition);
					auto palCopy = option->palette;
					painter->save();
					drawItemText(painter, textRect, textFlags, option->palette, enabled, accelerator, role);
					painter->restore();
				}

				// render text
				const int textFlags(Qt::AlignVCenter | (reverseLayout ? Qt::AlignRight : Qt::AlignLeft));
				textRect = option->fontMetrics.boundingRect(textRect, textFlags, text);
				drawItemText(painter, textRect, textFlags, option->palette, enabled, text, role);
			}
		}
		case QStyleOptionMenuItem::MenuItemType::EmptyArea:
			return;
		default:
			break;
		}

		return;
	}
	default:
		return QProxyStyle::drawControl(element, option, painter, widget);
	}
	return QProxyStyle::drawControl(element, option, painter, widget);
}

void KhemeStyle::drawPrimitive(PrimitiveElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget) const
{
	switch (element) {
	case PE_FrameMenu:
	case PE_PanelMenu: {
		const auto el = d->themeLoader()->theme()->menuStyle();

		BorderData dat;
		if (dat.updateFrom(el, propsFrom(option), d->getFor(widget), option->rect, option->palette)) {
			const_cast<QWidget*>(widget)->update();
		}
		dat.paintBackgroundTo(painter);
		dat.paintBorderTo(painter);

		return;
	}
	default:
		return QProxyStyle::drawPrimitive(element, option, painter, widget);
	}
}

QSize KhemeStyle::sizeFromContents(ContentsType type, const QStyleOption* option, const QSize& contentsSize, const QWidget* widget) const
{
	switch (type) {
	case CT_PushButton: {
		const auto el = d->themeLoader()->theme()->buttonStyle();
		auto padding = qSharedPointerObjectCast<Padding>(el->getAttributeShared("padding", false, false, false));
		return contentsSize.grownBy(padding->asMargins());
	}
	case CT_MenuItem: {
		const auto menuItemOption = qstyleoption_cast<const QStyleOptionMenuItem*>(option);
		ElementStyle* el;
		switch (menuItemOption->menuItemType) {
		case QStyleOptionMenuItem::MenuItemType::Separator:
			el = d->themeLoader()->theme()->menuSeparatorStyle();
			break;
		default:
			el = d->themeLoader()->theme()->menuItemStyle();
			break;
		}
		auto padding = qSharedPointerObjectCast<Padding>(el->getAttributeShared("padding", false, false, false));
		return contentsSize.grownBy(padding->asMargins());
	}
	default:
		return QProxyStyle::sizeFromContents(type, option, contentsSize, widget);
	}
}
