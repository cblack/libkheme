#pragma once

#include <QProxyStyle>
#include <QStyleOption>

class KhemeStyle : public QProxyStyle
{

	Q_OBJECT

	struct Private;
	QScopedPointer<Private> d;

public:
	explicit KhemeStyle(const QString& key);
	~KhemeStyle();

	inline QRect visualRect(const QStyleOption* opt, const QRect& subRect) const
	{
		return QProxyStyle::visualRect(opt->direction, opt->rect, subRect);
	}

	void drawControl(ControlElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget = nullptr) const override;
	void drawPrimitive(PrimitiveElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget = nullptr) const override;
	QSize sizeFromContents(ContentsType type, const QStyleOption* option, const QSize& contentsSize, const QWidget* widget = nullptr) const override;
};
