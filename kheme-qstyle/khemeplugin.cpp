#include "khemeplugin.h"
#include "khemestyle.h"

QStyle* KhemeStylePlugin::create(const QString& key)
{
	if (key == "kheme")
		return new KhemeStyle(key);

	return nullptr;
}
