QtApplication {
    files: [
        "*.cpp",
        "*.h",
    ]
    install: true
    installDir: "bin"
    Depends { name: "kheme" }
}
