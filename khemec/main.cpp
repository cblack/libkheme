#include <QCoreApplication>
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMetaProperty>

#include <libkheme.h>

#include "theme.h"

int main(int argc, char* argv[])
{
	QCoreApplication app(argc, argv);

	auto in = app.arguments()[1];
	auto out = app.arguments()[2];

	QFile f(in);
	if (!f.open(QIODevice::ReadOnly)) {
		app.exit(-1);
	}
	auto data = f.readAll();

	Kheme::ThemeCompiler c;
	auto theme = c.compileTheme(data, QUrl::fromUserInput(in));

	QFile of(out);
	if (!of.open(QIODevice::WriteOnly)) {
		app.exit(-1);
	}

	const auto serialized = theme->serialize();

	of.write(QJsonDocument(serialized).toJson());

	return 0;
}
