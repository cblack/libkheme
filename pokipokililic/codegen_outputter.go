package main

import (
	"fmt"
	"strings"
)

type outputter struct {
	strings.Builder
	indent uint
}

func (o *outputter) addsnl(format string, a ...interface{}) {
	if format != "" {
		o.WriteString(strings.Repeat("\t", int(o.indent)))
	}
	o.WriteString(fmt.Sprintf(format, a...))
}

func (o *outputter) addsnlsi(format string, a ...interface{}) {
	o.WriteString(fmt.Sprintf(format, a...))
}

func (o *outputter) addsi(format string, a ...interface{}) {
	o.WriteString(fmt.Sprintf(format, a...))
	o.WriteRune('\n')
}

func (o *outputter) add(format string, a ...interface{}) {
	if format != "" {
		o.WriteString(strings.Repeat("\t", int(o.indent)))
	}
	o.WriteString(fmt.Sprintf(format, a...))
	o.WriteRune('\n')
}

func (o *outputter) addO(format string, a ...interface{}) {
	o.add(format, a...)
	o.indent++
}

func (o *outputter) addC(format string, a ...interface{}) {
	o.indent--
	o.add(format, a...)
}
