package main

var imports = map[string]string{
	"QBitArray":          "QBitArray",
	"QBrush":             "QBrush",
	"QByteArray":         "QByteArray",
	"QColor":             "QColor",
	"QCursor":            "QCursor",
	"QDate":              "QDate",
	"QDateTime":          "QDateTime",
	"QEasingCurve":       "QEasingCurve",
	"QFont":              "QFont",
	"QGenericMatrix":     "QGenericMatrix",
	"QIcon":              "QIcon",
	"QImage":             "QImage",
	"QKeySequence":       "QKeySequence",
	"QMargins":           "QMargins",
	"QMatrix4x4":         "QMatrix4x4",
	"QPalette":           "QPalette",
	"QPen":               "QPen",
	"QPicture":           "QPicture",
	"QPixmap":            "QPixmap",
	"QPoint":             "QPoint",
	"QQuaternion":        "QQuaternion",
	"QRect":              "QRect",
	"QRegExp":            "QRegExp",
	"QRegularExpression": "QRegularExpression",
	"QRegion":            "QRegion",
	"QSize":              "QSize",
	"QString":            "QString",
	"QTime":              "QTime",
	"QTransform":         "QTransform",
	"QUrl":               "QUrl",
	"QVariant":           "QVariant",
	"QVector2D":          "QVector2D",
	"QVector3D":          "QVector3D",
	"QVector4D":          "QVector4D",
	"QLinkedList":        "QLinkedList",
	"QList":              "QList",
	"QVector":            "QVector",
	"QHash":              "QHash",
	"QMap":               "QMap",
	"QPair":              "QPair",
}

var scalarTypes = map[string]string{
	"Boolean":           "bool",
	"Int8":              "qint8",
	"Int16":             "qint16",
	"Int32":             "qint32",
	"Int64":             "qint64",
	"Uint8":             "quint8",
	"Uint16":            "quint16",
	"Uint32":            "quint32",
	"Uint64":            "quint64",
	"Float32":           "float",
	"Float64":           "double",
	"ConstChar":         "const char *",
	"BitArray":          "QBitArray",
	"Brush":             "QBrush",
	"ByteArray":         "QByteArray",
	"Color":             "QColor",
	"Cursor":            "QCursor",
	"Date":              "QDate",
	"DateTime":          "QDateTime",
	"EasingCurve":       "QEasingCurve",
	"Font":              "QFont",
	"GenericMatrix":     "QGenericMatrix",
	"Icon":              "QIcon",
	"Image":             "QImage",
	"KeySequence":       "QKeySequence",
	"Margins":           "QMargins",
	"Matrix4x4":         "QMatrix4x4",
	"Palette":           "QPalette",
	"Pen":               "QPen",
	"Picture":           "QPicture",
	"Pixmap":            "QPixmap",
	"Point":             "QPoint",
	"Quaternion":        "QQuaternion",
	"Rect":              "QRect",
	"RegExp":            "QRegExp",
	"RegularExpression": "QRegularExpression",
	"Region":            "QRegion",
	"Size":              "QSize",
	"String":            "QString",
	"Time":              "QTime",
	"Transform":         "QTransform",
	"URL":               "QUrl",
	"Variant":           "QVariant",
	"Vector2D":          "QVector2D",
	"Vector3D":          "QVector3D",
	"Vector4D":          "QVector4D",
}

var singularGenericTypes = map[string]string{
	"LinkedList": "QLinkedList",
	"List":       "QList",
	"Vector":     "QVector",
}

var dualGenericTypes = map[string]string{
	"Hash": "QHash",
	"Map":  "QMap",
	"Pair": "QPair",
}

func toImport(s string) (o string, ok bool) {
	if v, ok := scalarTypes[s]; ok {
		return imports[v], true
	} else if v, ok := singularGenericTypes[s]; ok {
		return imports[v], true
	} else if v, ok := dualGenericTypes[s]; ok {
		return imports[v], true
	}
	return "", false
}
