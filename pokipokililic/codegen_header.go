package main

func outputUnionStreamHeader(n string, k unionKind) string {
	o := outputter{}

	o.add(outTemplate+";", n+"*")

	// ----

	o.add(inTemplate+";", n+"*")

	for _, field := range k.fields {
		o.add(outputObjectStreamHeader(field.name, field.kind.(objectKind)))
	}

	return o.String()
}

func outputObjectStreamHeader(n string, k objectKind) string {
	o := outputter{}

	o.add(``)

	o.add(outTemplate+";", n+"*")

	// ---

	o.add(inTemplate+";", n+"*")

	return o.String()
}

func outputUnionHeader(n string, k unionKind) string {
	o := outputter{}

	o.addO(`class %s : public QObject {`, n)

	o.add(``)
	o.add(`Q_OBJECT`)
	o.add(`public: %s(QObject* parent = nullptr) : QObject(parent) {}`, n)
	o.add(`public: virtual ~%s() {}`, n)
	o.add(``)

	o.addC(`};`)

	for _, field := range k.fields {
		o.add(outputObjectHeader(field.name, n, field.kind.(objectKind)))
	}

	return o.String()
}

func outputObjectHeader(n, p string, k objectKind) string {
	o := outputter{}

	o.addO(`class %s : public %s {`, n, p)

	o.add(``)
	o.add(`Q_OBJECT`)
	o.add(``)

	// Q_PROPERTY decls
	for _, field := range k.fields {
		o.add(`Q_PROPERTY(%s %s READ %s WRITE set_%s NOTIFY %s_changed)`, resolveTypeDecl(field.kind), field.name, field.name, field.name, field.name)
	}

	o.add(``)

	o.add(`public: %s(QObject* parent = nullptr);`, n)

	o.add(``)

	// member decls
	for _, field := range k.fields {
		if isPointer(field.kind) {
			o.add(`private: QSharedPointer<%s> m_%s;`, extractPointerKind(field.kind), field.name)
		} else {
			o.add(`private: %s m_%s = {};`, resolveTypeDecl(field.kind), field.name)
		}
		o.add(`public: Q_SIGNAL void %s_changed();`, field.name)

		//

		o.add(`public: %s %s() const;`, resolveTypeDecl(field.kind), field.name)

		//

		o.add(`public: void set_%s(%s value);`, field.name, resolveTypeDecl(field.kind))

		//

		o.add(``)
	}

	o.addC(`};`)

	return o.String()
}

func outputClassHeader(kind namedKind) string {
	switch it := kind.k.(type) {
	case unionKind:
		return outputUnionHeader(kind.name, it)
	case objectKind:
		return outputObjectHeader(kind.name, "QObject", it)
	default:
		panic("unhandled")
	}
}
