Product {
    name: "pokipokililic"
    type: "go_binary"

    Group {
        files: [
            "*.go",
        ]
        fileTags: "go_files"
    }

    Rule {
        multiplex: true
        inputs: ["go_files"]
        Artifact {
            filePath: product.name
            fileTags: ["go_binary"]
        }
        prepare: {
            var command = new Command("go", ["build", "-o", output.filePath])
            command.workingDirectory = product.sourceDirectory
            command.description = "Compiling " + output.fileName
            command.highlight = "compiler"
            return [command];
        }
    }
}