# data format

unions:
```
qint16     T...
type tag   the data of the type
```

objects:
```
qint16     [ qint16      T...                         ]*
size tag     field tag   the data of the field's type
```
