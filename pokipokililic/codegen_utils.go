package main

import (
	"fmt"
	"log"
)

func extractPointerKind(k kind) string {
	data, ok := k.(primitiveKind)
	if !ok {
		log.Panicf("bad type %+v", k)
	}
	if len(data.typeArgs) != 0 {
		panic("can't be a pointer")
	}
	return data.primitive
}

func isPointer(k kind) bool {
	data, ok := k.(primitiveKind)
	if !ok {
		log.Panicf("bad type %+v", k)
	}
	if len(data.typeArgs) != 0 {
		return false
	}
	val := scalarTypes[data.primitive]
	return val == ""
}

func resolveTypeDecl(k kind) string {
	data, ok := k.(primitiveKind)
	if !ok {
		log.Panicf("bad type %+v", k)
	}
	switch {
	case len(data.typeArgs) == 0:
		val := scalarTypes[data.primitive]
		if val != "" {
			return val
		}
		return fmt.Sprintf(`%s*`, data.primitive)
	case len(data.typeArgs) == 1:
		val := singularGenericTypes[data.primitive]
		if val == "" {
			log.Panicf("%s does not have a type argument", data.primitive)
		}
		return val
	case len(data.typeArgs) == 2:
		val := dualGenericTypes[data.primitive]
		if val == "" {
			log.Panicf("%s does not have a type argument", data.primitive)
		}
		return val
	default:
		log.Panicf("too many type args in %+v", k)
	}
	for {
	}
}
