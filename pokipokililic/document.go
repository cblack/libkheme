package main

type kind interface{ isKind() }
type namedKind struct {
	name string
	k    kind
}

//

type primitiveKind struct {
	primitive string
	typeArgs  []kind
}

func (primitiveKind) isKind() {}

//

type field struct {
	name   string
	number int32
	kind   kind
}

type objectKind struct {
	fields []field
}

func (objectKind) isKind() {}

//

type unionKind struct {
	fields []field
}

func (unionKind) isKind() {}

//

type document struct {
	kinds []namedKind
}
