package main

import (
	"fmt"
	"path/filepath"
	"sort"
	"strings"
)

const (
	outTemplate = "QDataStream& operator<<(QDataStream& out, %s const& v)"
	inTemplate  = "QDataStream& operator>>(QDataStream& in, %s& v)"
)

func outputClassImpls(kind namedKind) string {
	switch it := kind.k.(type) {
	case unionKind:
		return outputUnionImpl(kind.name, it)
	case objectKind:
		return outputObjectImpl(kind.name, "QObject", it)
	default:
		panic("unhandled")
	}
}

func outputStreamImpls(kind namedKind) string {
	switch it := kind.k.(type) {
	case unionKind:
		return outputUnionStreamImpl(kind.name, it)
	case objectKind:
		return outputObjectStreamImpl(kind.name, it)
	default:
		panic("unhandled")
	}
}

func outputStreamHeaders(kind namedKind) string {
	switch it := kind.k.(type) {
	case unionKind:
		return outputUnionStreamHeader(kind.name, it)
	case objectKind:
		return outputObjectStreamHeader(kind.name, it)
	default:
		panic("unhandled")
	}
}

func outputImpl(kinds []namedKind) string {
	s := []string{
		fmt.Sprintf(`#include "%s"`, filepath.Base(outputHeaderPath)),
	}

	for _, k := range kinds {
		s = append(s, outputClassImpls(k))
	}
	for _, k := range kinds {
		s = append(s, outputStreamImpls(k))
	}

	return strings.Join(s, "\n")
}

func outputHeader(kinds []namedKind) string {
	s := []string{
		"#pragma once",
		"",
		"// pokipokilili imports",
		"#include <QDataStream>",
		"#include <QDebug>",
		"#include <QSharedPointer>",
		"",
		"// user type imports",
	}

	userimports := map[string]struct{}{}
	for _, k := range kinds {
		switch it := k.k.(type) {
		case unionKind:
			for _, f := range it.fields {
				for _, field := range f.kind.(objectKind).fields {
					prim, ok := field.kind.(primitiveKind)
					if !ok {
						continue
					}
					if v, ok := imports[prim.primitive]; ok {
						userimports[v] = struct{}{}
					}
				}
			}
		case objectKind:
			for _, field := range it.fields {
				prim, ok := field.kind.(primitiveKind)
				if !ok {
					continue
				}
				if v, ok := toImport(prim.primitive); ok {
					userimports[v] = struct{}{}
				}
			}
		default:
			panic("unhandled")
		}
	}

	userimportsarr := []string{}
	for k := range userimports {
		if k == "" {
			continue
		}
		userimportsarr = append(userimportsarr, k)
	}
	sort.Strings(userimportsarr)

	for _, it := range userimportsarr {
		s = append(s, fmt.Sprintf(`#include <%s>`, it))
	}

	s = append(s, "")

	classes := []string{}
	for _, k := range kinds {
		classes = append(classes, k.name)
		switch it := k.k.(type) {
		case unionKind:
			for _, f := range it.fields {
				classes = append(classes, f.name)
			}
		}
	}
	sort.Strings(classes)

	for _, cls := range classes {
		s = append(s, fmt.Sprintf("class %s;", cls))
	}

	for _, k := range kinds {
		s = append(s, outputClassHeader(k))
	}

	s = append(s, "// ---- begin streams ----")

	for _, k := range kinds {
		s = append(s, outputStreamHeaders(k))
	}

	return strings.Join(s, "\n")
}
