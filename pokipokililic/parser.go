package main

import (
	"bytes"
	"io/ioutil"
	"path"
	"text/scanner"
)

type parser struct {
	s customScanner
}

func (p *parser) parsePrimitive() (pr primitiveKind) {
	pr.primitive = p.s.ScanIdent()
	if p.s.Peek() == '[' {
		p.s.Scan()
		for p.s.Peek() != ']' {
			pr.typeArgs = append(pr.typeArgs, p.parseKind(false, true))
		}
		p.s.Scan()
	}
	return pr
}

func (p *parser) parseUnion() (u unionKind) {
	p.s.ScanExpecting("[")

	p.s.SkipWS()
	for p.s.Peek() != ']' {
		u.fields = append(u.fields, field{
			name:   p.s.ScanIdent(),
			number: int32(p.s.ScanNumber()),
			kind:   p.parseObject(),
		})
		p.s.SkipWS()
	}

	p.s.ScanExpecting("]")

	return u
}

func (p *parser) parseObject() (o objectKind) {
	p.s.ScanExpecting("[")

	p.s.SkipWS()
	for p.s.Peek() != ']' {
		o.fields = append(o.fields, field{
			name:   p.s.ScanIdent(),
			number: int32(p.s.ScanNumber()),
			kind:   p.parseKind(false, true),
		})
		p.s.SkipWS()
	}

	p.s.ScanExpecting("]")

	return o
}

func (p *parser) parseKind(allowExtended, allowPrim bool) kind {
	kinds := []string{}
	if allowExtended {
		kinds = append(kinds, "object", "union")
	}
	if allowPrim {
		kinds = append(kinds, "@")
	}

	expecting := p.s.ScanExpectingOneOf(kinds...)

	switch expecting {
	case "object":
		return p.parseObject()
	case "union":
		return p.parseUnion()
	case "@":
		return p.parsePrimitive()
	default:
		panic(expecting)
	}
}

func (p *parser) parseType() namedKind {
	p.s.ScanExpecting("type")
	id := p.s.ScanIdent()
	return namedKind{id, p.parseKind(true, false)}
}

func parseFile(f string) (*document, error) {
	data, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, err
	}

	p := parser{}
	p.s.Init(bytes.NewReader(data))
	p.s.Filename = path.Base(f)

	d := &document{}

	for p.s.Peek() != scanner.EOF {
		d.kinds = append(d.kinds, p.parseType())
		p.s.SkipWS()
	}

	return d, nil
}
