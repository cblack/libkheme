package main

import "os"
import "io/ioutil"

var (
	inputPath        = os.Args[1]
	outputHeaderPath = os.Args[2]
	outputImplPath   = os.Args[3]
)

func main() {
	d, err := parseFile(inputPath)
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(outputHeaderPath, []byte(outputHeader(d.kinds)), os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(outputImplPath, []byte(outputImpl(d.kinds)), os.ModePerm)
	if err != nil {
		panic(err)
	}
	os.Stdout.WriteString(outputImpl(d.kinds))
	os.Stderr.WriteString(outputHeader(d.kinds))
}
