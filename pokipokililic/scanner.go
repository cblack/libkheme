package main

import (
	"log"
	"regexp"
	"strconv"
	"text/scanner"
	"unicode"
)

type customScanner struct {
	scanner.Scanner
}

func (s *customScanner) ScanNoEOF() rune {
	ret := s.Scanner.Scan()
	if ret == scanner.EOF {
		log.Panicf("%s: Unexpected EOF", s.Position)
	}
	return ret
}

func (s *customScanner) ScanIdent() string {
	s.ScanNoEOF()
	if !isIdent(s.TokenText()) {
		log.Panicf("%s: '%s' is not a valid identifier", s.Position, s.TokenText())
	}
	return s.TokenText()
}

func (s *customScanner) SkipWS() {
	for unicode.IsSpace(s.Peek()) {
		s.Next()
	}
}

func (s *customScanner) ScanExpecting(str string) string {
	s.ScanNoEOF()
	if s.TokenText() != str {
		log.Fatalf("%s: Was expecting '%s', got '%s'", s.Position, str, s.TokenText())
	}
	return s.TokenText()
}

func (s *customScanner) ScanExpectingOneOf(strs ...string) string {
	s.ScanNoEOF()
	scanned := s.TokenText()
	for _, str := range strs {
		if str == scanned {
			return scanned
		}
	}
	log.Fatalf("%s: Was expecting one of %+v, got '%s'", s.Position, strs, s.TokenText())
	return s.TokenText()
}

func (s *customScanner) ScanToEOL() (ret []string) {
	for s.Peek() != '\n' {
		s.ScanNoEOF()
		ret = append(ret, s.TokenText())
	}
	return
}

func (s *customScanner) ScanNumber() int64 {
	s.ScanNoEOF()
	if !isNumber(s.TokenText()) {
		log.Fatalf("%s: Was expecting a number, got '%s'", s.Position, s.TokenText())
	}
	ret, _ := strconv.ParseInt(s.TokenText(), 10, 64)
	return ret
}

var isIdent = regexp.MustCompile(`^[a-zA-Z][a-zA-Z0-9]+$`).MatchString
var isNumber = regexp.MustCompile(`^[0-9]+$`).MatchString
