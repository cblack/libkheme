package main

func outputUnionStreamImpl(n string, k unionKind) string {
	o := outputter{}

	o.addO(outTemplate+" {", n+"*")

	for _, field := range k.fields {
		o.addO(`if (auto obj = dynamic_cast<%s*>(v)) {`, field.name)
		o.add(`out << qint16(%d) << obj;`, field.number)
		o.addC(`}`)
	}

	o.add(`return out;`)
	o.addC(`}`)

	// ----

	o.addO(inTemplate+" {", n+"*")
	o.add(`qint16 tag;`)
	o.add(`in >> tag;`)
	o.add(``)

	o.add(`switch (tag) {`)
	for _, field := range k.fields {
		o.addO(`case %d: {`, field.number)
		o.add(`%s* out = nullptr;`, field.name)
		o.add(`in >> out;`)
		o.add(`v = out;`)
		o.add(`break;`)
		o.addC(`}`)
	}
	o.add(`}`)

	o.add(``)
	o.add(`return in;`)
	o.addC(`}`)

	for _, field := range k.fields {
		o.add(outputObjectStreamImpl(field.name, field.kind.(objectKind)))
	}

	return o.String()
}

func outputObjectStreamImpl(n string, k objectKind) string {
	o := outputter{}

	o.add(``)

	o.addO(outTemplate+" {", n+"*")
	o.addsnl(`out << qint16(%d)`, len(k.fields))
	for _, field := range k.fields {
		o.addsnlsi(` << qint16(%d) << v->%s()`, field.number, field.name)
	}
	o.addsi(`;`)
	o.add(`return out;`)
	o.addC(`}`)

	// ---

	o.addO(inTemplate+" {", n+"*")

	o.add(`%s* ret = new %s;`, n, n)
	o.add(``)

	o.add(`qint16 taglen;`)
	o.add(`in >> taglen;`)

	o.addO(`for (qint16 i = 0; i < taglen; i++) {`)

	o.add(`qint16 tag;`)
	o.add(`in >> tag;`)

	o.add(`switch (tag) {`)
	for _, field := range k.fields {
		o.addO(`case %d: {`, field.number)
		o.add(`%s val;`, resolveTypeDecl(field.kind))
		o.add(`in >> val;`)
		o.add(`ret->set_%s(val);`, field.name)
		o.add(`break;`)
		o.addC(`}`)
	}
	o.addO(`default: {`)
	o.add(`qCritical() << "unrecognised tag" << tag << "parsing a %s! aborting parsing...";`, n)
	o.add(`v = nullptr;`)
	o.add(`return in;`)
	o.addC(`}`)
	o.add(`}`)

	o.addC(`}`)

	o.add(``)
	o.add(`v = ret;`)
	o.add(``)
	o.add(`return in;`)
	o.addC(`}`)

	return o.String()
}

func outputUnionImpl(n string, k unionKind) string {
	o := outputter{}

	for _, field := range k.fields {
		o.add(outputObjectImpl(field.name, n, field.kind.(objectKind)))
	}

	return o.String()
}

func outputObjectImpl(n, p string, k objectKind) string {
	o := outputter{}

	o.addO(`%s::%s(QObject* parent) : %s(parent) {`, n, n, p)
	for _, field := range k.fields {
		if isPointer(field.kind) {
			o.add(`m_%s.reset(new %s);`, field.name, extractPointerKind(field.kind))
		}
	}
	o.addC(`}`)

	// member decls
	for _, field := range k.fields {

		o.addsnl(`%s %s::%s() const { `, resolveTypeDecl(field.kind), n, field.name)

		if isPointer(field.kind) {
			o.addsnlsi(`return m_%s.data();`, field.name)
		} else {
			o.addsnlsi(`return m_%s;`, field.name)
		}

		o.addsi(` }`)

		//

		o.addsnl(`void %s::set_%s(%s value) { `, n, field.name, resolveTypeDecl(field.kind))

		if isPointer(field.kind) {
			o.addsnlsi(`m_%s.reset(value);`, field.name)
		} else {
			o.addsnlsi(`m_%s = value;`, field.name)
		}

		o.addsi(` Q_EMIT %s_changed(); }`, field.name)

		//

		o.add(``)
	}

	return o.String()
}
